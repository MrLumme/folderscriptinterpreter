package lumCode.folderScriptInterpreter.exceptions;

import lumCode.folderScriptInterpreter.handlers.command.CommandType;

public class IncorrectParameterAmountException extends InterpreterException {
	private static final long serialVersionUID = 1909083068084599020L;

	public IncorrectParameterAmountException(CommandType c, int amount) {
		super("The command '" + c.getChar() + "' expects " + c.getInput() + " input parameters, but has gotten "
				+ amount + ".");
	}
}
