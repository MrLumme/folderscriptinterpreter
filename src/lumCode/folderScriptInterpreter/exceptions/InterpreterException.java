package lumCode.folderScriptInterpreter.exceptions;

public class InterpreterException extends Exception {
	private static final long serialVersionUID = 4169904615141147003L;

	public InterpreterException(String message) {
		super(message);
	}
}
