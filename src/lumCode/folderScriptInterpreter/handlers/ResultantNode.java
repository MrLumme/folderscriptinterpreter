package lumCode.folderScriptInterpreter.handlers;

import lumCode.folderScriptInterpreter.variables.Variable;

public interface ResultantNode extends Node {

	public Variable result();
}
